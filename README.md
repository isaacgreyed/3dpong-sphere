# 3dpong-sphere

A simple game where you bounce a projectile back and forth. If you are the next once to miss an oponent gains a point. 

Currently has basic AI, a working score system, multiple camera angles simultaneously. Potentially looking to add support for playing along the shape of an arbitrary 3d model.
